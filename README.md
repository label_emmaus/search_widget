Emmaus widget
=====================



## AlgoliaSearch configuration
create a settings.js file with the following object :
```
module.exports = {
    algoliasearchCredentials : {
        applicationId: "###YOUR_APPLICATION_ID###",
        searchAPIKey: "###YOUR_SEARCH_API_KEY###"
    }
};
```

### Run the widget for development

```
npm install
npm start
open http://localhost:8080
```

### Build a production-ready JavaScript-bundle widget

```
npm run build
```
