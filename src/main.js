import App from './components/App.html';
import algoliasearch from 'algoliasearch/lite';
import { algoliasearchCredentials } from '../settings.js';

const targets = document.querySelectorAll('.lbe_widget_v1');
for (let i = 0; i < targets.length; ++i) {
    const target = targets[i];

    // Evite de process la cible plusieurs fois.
     if (target.getAttribute('data-processed')) {
         continue;
     }
     target.setAttribute('data-processed', 1);

    // Empty the container.
    while (target.hasChildNodes()) {
        target.removeChild(target.firstChild);
    }

    let data = {};
    const keys = ['options', 'limit', 'width', 'height', 'merchants', 'categories'];
    for (const i in keys) {
        data[keys[i]] = target.getAttribute('data-' + keys[i]);
    }

    // Redimentionnement du widget.
    if (data.width > 0) {
        target.style.width = data.width + "px";
    }
    if (data.height > 0) {
        target.style.height = data.height + "px";
    }

    // Initialisation du filtre par catégories.
    let category_ids = [];
    if (data.merchants) {
        category_ids = category_ids.concat(data.merchants.split(','))
    }
    if (data.categories) {
        category_ids = category_ids.concat(data.categories.split(','))
    }

    let facetFilters = [];
    category_ids.forEach (function (category_id) {
        facetFilters.push('category_ids:' + category_id);
    });

    data.filters = facetFilters;
    let options = [];
    if (data.options) {
      options = data.options.split(',');
    }
    data.options = {};
    for (const i in options) {
      let option = options[i];
      if (typeof option == 'string' && option.length) {
          option = option.trim();
          data.options[option] = true;
        }
    }

    data.size = 'grandangle';
    if (target.style.height != undefined && target.style.height != '') {
        const targetRect = target.getBoundingClientRect();
        if (targetRect.height < 500) {
            data.options['nosearch'] = true;
        }
        if (targetRect.height < 250) {
            data.size = 'megaban';
        }
        else if (targetRect.height < 300) {
            data.size = 'pave';
        }
    }

    data.queryString = '?'
        + 'utm_source=' + encodeURIComponent(window.location.hostname)
        + '&utm_medium=widget'
        + '&utm_campaign=format_' + data.size
        + '&utm_content=' + encodeURIComponent(category_ids.join(','));

    // Initialisation du client de recherche Algolia.
    const client = algoliasearch(algoliasearchCredentials.applicationId, algoliasearchCredentials.searchAPIKey);
    const index = client.initIndex('lbe_prod_fr_products');
    data.index = index;

    const app = new App({
        target: target,
        data: data
    });

}
